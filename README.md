dllu cellular automaton symmetric key stream cipher
===========================================

Simple symmetric key stream cipher operating on 6 bit blocks, which is probably not very strong. When used for trivial things, most attackers would probably be too lazy to try to break it due to how trivial the encrypted message is, even if they have access to the source code.

The method of operation is:

* compute base64 version (i.e. 6 bit blocks) of input data
* use the key to seed the initial state of a cellular automaton using SHA1
* advance the cellular automaton while reading values from it
* add those pseudorandom values to the data modulo 64

The cellular automaton uses a 3D lattice of size 10 × 8 × 8, with 64 states per site. The same rule of a neighborhood of size 3 (i.e. 64^3 = 262144 total size of rulestring) is applied sequentially in each dimension. We use 16 different cellular automaton rules and take the sum of their pseudorandom output (modulo 64).

Unfortunately, my algorithm is **very slow** and its effectiveness is unproven.

**Conjecture**: As the number of states of such a cellular automaton is increased, the proportion of rules with a "Type 3" (random-looking) behaviour from a randomly seeded initial state rapidly approaches 1. No attempt shall be made here to prove this conjecture. Using 16 different randomly selected rules ensures with high probability that some of them would be quite random.

For k states, the number of rulestrings is k^(k^3) which is really huge when k is 64. The number of states of our cellular automaton is k^(10 × 8 × 8) which is also really huge. Thus it is difficult to brute force the state of the cellular automata. Nonetheless, there may be exploitable patterns in the cellular automata, so I urge you _do not use this for critical tasks_. This is strictly a hobby project.

This only works in Python 2 because Python 3 treats strings as unicode.