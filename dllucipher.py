#!/usr/bin/python
import base64
import cipherpad
import sys
usagemessage = """USAGE: python dllucipher.py (encrypt|decrypt) key
This script reads and writes on standard IO
e.g.   python dllucipher.py encrypt "zxcv qwer" < plaintext.txt > encrypted.txt
       python dllucipher.py decrypt "zxcv qwer" < encrypted.txt > plaintext.txt
"""

def encrypt(key, text):
    (text64, e1, e2) = base64.b64encode(text).partition('=')
    text64 = [cipherpad.char2int[x] for x in text64]
    otp = cipherpad.cipherpad(key, len(text64))
    text64 = [(j + otp[i]) & 63 for i,j in enumerate(text64)]
    return ''.join([cipherpad.charset[x] for x in text64]) + e1 + e2

def decrypt(key, text64):
    (text64, e1, e2) = text64.partition('=')
    text64 = [cipherpad.char2int[x] for x in text64]
    otp = cipherpad.cipherpad(key, len(text64))
    text64 = [(j + 64 - otp[i]) & 63 for i,j in enumerate(text64)]
    return base64.b64decode(
        ''.join([cipherpad.charset[x] for x in text64]) + e1 + e2)

def main(argv=None):
    if argv is None:
        argv = sys.argv
    s = sys.stdin.read()
    if argv[1] == 'encrypt':
        sys.stderr.write('Encrypting using key: ' + argv[2] + '\n')
        print(encrypt(argv[2], s))
    elif argv[1] == 'decrypt':
        sys.stderr.write('Decrypting using key: ' + argv[2] + '\n')
        print(decrypt(argv[2], s))
    else:
        sys.stderr.write(usagemessage)

if __name__ == "__main__":
    sys.exit(main())
