import random
import hashlib
import copy
import rules
import array

charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
char2int = {charset[i]:i for i in range(64)}
hexset = "0123456789abcdef"
hex2int = {hexset[i]:i for i in range(16)}
(x, y, z) = (10, 8, 8)

def advance(lattice, rule):
    # advance the cellular automaton, which is 3D with 64 states
    L1 = lattice
    L2 = copy.deepcopy(lattice)
    L3 = copy.deepcopy(lattice)
    for i in range(x):
        for j in range(y):
            for k in range(z):
                a = L1[(i+x-1)%x + j*x + k*x*y]
                b = L1[ i        + j*x + k*x*y]
                c = L1[(i+1  )%x + j*x + k*x*y]
                L2[i + j*x + k*x*y] = rule[a + 64*b + 64*64*c]
    for i in range(x):
        for j in range(y):
            for k in range(z):
                a = L2[i + ((j+y-1)%y)*x + k*x*y]
                b = L2[i + ( j       )*x + k*x*y]
                c = L2[i + ((j+1  )%y)*x + k*x*y]
                L3[i + j*x + k*x*y] = rule[a + 64*b + 64*64*c]
    for i in range(x):
        for j in range(y):
            for k in range(z):
                a = L3[i + j*x + ((k+z-1)%z)*x*y]
                b = L3[i + j*x + ( k       )*x*y]
                c = L3[i + j*x + ((k+1  )%z)*x*y]
                L1[i + j*x + k*x*y] = rule[a + 64*b + 64*64*c]

def cipherpad(key, n):
    P =[96, 43, 55, 292, 230, 347, 40, 425, 322, 152, 166, 257, 556, 458, 229,
176, 479, 287, 495, 326, 172, 275, 266, 380, 20, 543, 192, 286, 224, 387,
544, 522, 442, 453, 424, 162, 191, 285, 580, 550, 396, 178, 133, 390, 194, 272,
32, 582, 399, 59, 535, 406, 434, 411, 283, 370, 270, 381, 294, 57, 87, 136, 538,
234, 200, 233, 592, 595, 179, 112, 308, 413, 561, 571, 414, 81, 500, 364, 558,
145, 265, 352, 433, 187, 309, 583, 401, 527, 463, 91, 353, 284, 432, 126, 471,
150, 391, 589, 127, 232, 316, 290, 409, 533, 358, 163, 164, 249, 312, 596, 213,
242, 115, 206, 502, 507, 123, 186, 184, 240, 44, 129, 250, 388, 473, 477, 586,
303, 121, 2, 105, 253, 74, 83, 608, 628, 137, 405, 525, 260, 341, 615, 360, 89,
167, 157, 602, 35, 503, 36, 52, 557, 555, 506, 204, 383, 248, 332, 33, 317, 457,
618, 436, 236, 299, 185, 455, 452, 599, 467, 46, 371, 384, 62, 118, 613, 552,
372, 109, 560, 68, 420, 509, 100, 267, 483, 497, 183, 77, 24, 138, 122, 469,
553, 620, 156, 141, 484, 459, 282, 448, 110, 165, 579, 537, 160, 78, 306, 519,
584, 130, 159, 532, 567, 605, 430, 539, 348, 549, 466, 493, 318, 139, 84, 480,
180, 395, 577, 305, 295, 65, 510, 304, 201, 475, 274, 107, 426, 51, 95, 146,
614, 143, 174, 199, 516, 76, 440, 600, 258, 576, 116, 398, 505, 578, 627, 329,
244, 524, 177, 61, 254, 365, 534, 104, 422, 546, 634, 389, 375, 217, 518, 630,
54, 504, 30, 114, 636, 573]
    # use SHA1 to generate a string of length 640 (base 64)
    seed = ''.join([hashlib.sha1(key + str(i)).hexdigest() for i in range(32)])
    seed = [(hex2int[seed[2*i]]*16 + hex2int[seed[2*i+1]]) & 63 for i in range(640)]
    # first start with a vigenere of period 640 just for the lolz
    # and in case my CA isn't as strong as I thought it was
    output = (seed*(n//640+1))[:n]
    seed = array.array('i', seed)
    for r in range(16):
        # rule is of length 262144
        rule = array.array('i', rules.RULE[r])
        # salt the rule
        for q in range(262144):
            rule[q] = (rule[q] + seed[q & 511]) & 63
        lattice = copy.deepcopy(seed)
        for q in range(10): # before we do anything else, advance 10 iterations
            advance(lattice, rule)
        for q in range(n//len(P)+1):
            advance(lattice, rule)
            # to speed things up we sample len(P) numbers per iteration
            for i, p in enumerate(P):
                if q*len(P) + i >= n:
                    break
                output[q*len(P) + i] += lattice[p]
    return [o%64 for o in output]
